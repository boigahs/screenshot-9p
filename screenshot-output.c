#include <pthread.h>
#include <u.h>
#include <libc.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <obs/obs.h>
#include <obs/obs-output.h>
#include <obs/util/threading.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#include "data.h"

extern void *handle9p(void *data);

static uint pngbufsize = 8192;

static const char *
getname(void *unused){
	UNUSED_PARAMETER(unused);
	return "ScreenshotOutput";
}

static void *
screate(obs_data_t *settings, obs_output_t *output){
	ssdata *d = bzalloc(sizeof (ssdata));

	d->output = output;
	d->conv = bzalloc(sizeof (struct video_scale_info));
	d->data = bzalloc(pngbufsize);
	d->w = obs_output_get_width(output);
	d->h = obs_output_get_height(output);
	d->sname = obs_data_get_string(settings, "sname");
	pipe(d->p);
	os_event_init(&d->ready, OS_EVENT_TYPE_MANUAL);

	d->conv->width = d->w;
	d->conv->height = d->h;
	d->conv->format = VIDEO_FORMAT_RGBA;
	d->conv->range = VIDEO_RANGE_DEFAULT;
	d->conv->colorspace = VIDEO_CS_DEFAULT;
	obs_output_set_video_conversion(output, d->conv);
	return d;
}

static void
destroy(void *data){
	ssdata *d = data;
	if(d){
		printf("Destroying output!\n");
		pngbufsize = 8192;
		close(d->p[0]);
		bfree(d->conv);
		bfree(d->data);
		bfree(d);
	}
}

static bool
start(void *data){
	ssdata *d = data;
	if(pthread_create(&d->ht, NULL, handle9p, data)) return false;
	return true;
}

static void
stop(void *data, uint64_t ts){
	ssdata *d = data;
	obs_output_end_data_capture(d->output);
	pthread_cancel(d->ht);
	UNUSED_PARAMETER(ts);
}

static void
senddata(void *context, void *data, int size){
	ssdata *d = context;
	if(size > pngbufsize) d->data = brealloc(d->data, size);
	memcpy(d->data, data, size);
	d->size = size;
	print("Sending PNG (Size %d)\n", d->size);
	os_event_signal(d->ready);
}

static void
rawvideo(void *data, struct video_data *frame){
	ssdata *d = data;
	stbi_write_png_to_func(senddata, d, d->w, d->h, 4, *frame->data, d->w*4);
	obs_output_end_data_capture(d->output);
}

static void
getdefaults(obs_data_t *defs){
	obs_data_set_default_string(defs, "sname", "obsshot");
}

static obs_properties_t *
getprops(void *data){
	UNUSED_PARAMETER(data);
	obs_properties_t *r = obs_properties_create();
	obs_properties_add_text(r, "sname", "9P Service Name", OBS_TEXT_DEFAULT);
	return r;
}

struct obs_output_info screenshot_output = {
	.id = "screenshot_output",
	.flags = OBS_OUTPUT_VIDEO,
	.get_name = getname,
	.create = screate,
	.destroy = destroy,
	.start = start,
	.stop = stop,
	.raw_video = rawvideo,
	.get_defaults = getdefaults,
	.get_properties = getprops,
};
	