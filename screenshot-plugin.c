#include <stdio.h>
#include <obs/obs-module.h>

OBS_DECLARE_MODULE()

extern struct obs_output_info screenshot_output;
obs_output_t *output;

bool
obs_module_load(void){
	obs_data_t *settings = obs_data_create();
	obs_register_output(&screenshot_output);
	output = obs_output_create("screenshot_output", "screenshot_output", settings, NULL);
	obs_data_release(settings);
	obs_output_start(output);
	return true;
}

void
obs_module_unload(void){
	printf("screenshot: output destroyed\n");
}
