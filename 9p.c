#include <pthread.h>
#include <u.h>
#include <libc.h>
#include <fcall.h>
#include <obs/obs.h>
#include <obs/obs-output.h>
#include <obs/util/threading.h>
#include "data.h"


enum{
	MAXDSIZE = 8192,
};

typedef struct Fid Fid;
struct Fid {
	uint fid;
	Qid *qid;
	Dir *dir;
	char open;
	char used;
	Fid *next;
};

char *rversion(Fid*), *rauth(Fid*), *rflush(Fid*), *rattach(Fid*), *rwalk(Fid*), *ropen(Fid*),
	*rcreate(Fid*), *rread(Fid*), *rwrite(Fid*), *rclunk(Fid*), *rremove(Fid*), *rstat(Fid*), *rwstat(Fid*);
char *(*rfuncs[Tmax])(Fid*) = {
	[Tversion] = rversion,
	[Tauth] = rauth,
	[Tflush] = rflush,
	[Tattach] = rattach,
	[Twalk] = rwalk,
	[Topen] = ropen,
	[Tcreate] = rcreate,
	[Tread] = rread,
	[Twrite] = rwrite,
	[Tclunk] = rclunk,
	[Tremove] = rremove,
	[Tstat] = rstat,
	[Twstat] = rwstat,
};

static Dir root, png;
static uchar rootstat[STATMAX], pngstat[STATMAX];
static u32int nrootstat, npngstat;

static os_event_t *pngready;
static char **pngdatapp;
static u32int *pngsizep;
obs_output_t *output;

static u32int msize = IOHDRSZ + MAXDSIZE;
static uchar mdata[IOHDRSZ + MAXDSIZE];
static Fcall tmsg, rmsg;
static Fid *fids;

static Fid *
newfid(uint fid){
	Fid *f, *ff = nil;

	for(f = fids; f; f = f->next){
		if(f->fid == fid){
			print("Existing FID #%d (%s) requested\n", fid, f->used ? "Used" : "Not Used");
			return f;
		}
		else if(!ff && !f->used) ff = f;
	}
	if(ff){
		print("Unused FID #%d taken by %d\n", ff->fid, fid);
		ff->fid = fid;
		return ff;
	}
	print("New FID #%d allocated\n", fid);
	f = calloc(sizeof (Fid), 1);
	f->fid = fid;
	f->next = fids;
	fids = f;
	return f;
}

char *
rversion(Fid *x){
	Fid *f;
	if(strcmp(tmsg.version, "9P2000") != 0) return "unsupported version";
	else rmsg.version = "9P2000";
	rmsg.msize = msize = tmsg.msize > IOHDRSZ+MAXDSIZE ? IOHDRSZ+MAXDSIZE : tmsg.msize;
	for(f = fids; f; f = f->next) if(f->used) rclunk(f);
	return nil;
}

char *
rauth(Fid *f){
	if(f->used) return "rauth: bad fid";
	return "no authentication needed";
}

char *
rflush(Fid *f){
	return nil;
}

char *
rattach(Fid *f){
	if(f->used) return "rattach: bad fid";
	if(tmsg.afid != NOFID) return "bad afid";
	f->dir = &root;
	f->qid = &root.qid;
	f->used = 1;
	rmsg.qid = root.qid;
	return nil;
}

char *
rwalk(Fid *f){
	int i;
	Fid *nf;

	if(f->open) return "walk on open fid";
	if(!f->used){
		print("Walking unused FID #%d\n", f->fid);
		return "rwalk: bad fid";
	}
	if(tmsg.nwname > MAXWELEM) return "too many wnames";

	if(tmsg.newfid != f->fid){
		nf = newfid(tmsg.newfid);
		if(nf->used) return "bad newfid";
		nf->dir = f->dir;
		nf->qid = f->qid;
		nf->used = 1;
	}
	else nf = f;

	for(i = rmsg.nwqid = 0; i < tmsg.nwname; i++, rmsg.nwqid++){
		if(nf->qid == &root.qid){
			if(strcmp(tmsg.wname[i], "..") == 0) rmsg.wqid[i] = root.qid;
			else if(strcmp(tmsg.wname[i], "png") == 0){
				rmsg.wqid[i] = png.qid;
				nf->dir = &png;
				nf->qid = &png.qid;
			}
		}
		else if(nf->qid == &png.qid) break;
	}
	if(rmsg.nwqid < tmsg.nwname){
		nf->qid = nil;
		print("rwalk: clunking fid (%d) of failed walk\n", f->fid);
		nf->used = 0;
		if(i == 0) return "tried to walk non-directory";
	}
	return nil;
}

char *
ropen(Fid *f){
	uchar mode, trunc, rclose;
	if(!f->used) return "ropen: bad fid";

	mode = tmsg.mode & 0x3;
	trunc = tmsg.mode & OTRUNC;
	rclose = tmsg.mode &ORCLOSE;
	if(f->qid == &root.qid){
		if(mode == OWRITE || mode == ORDWR || trunc || rclose) return "no writing allowed";
		f->open = 1;
		rmsg.qid = root.qid;
		rmsg.iounit = msize-IOHDRSZ;
		return nil;
	}
	if(f->qid == &png.qid){
		if(mode != OREAD || trunc || rclose) return "no writing allowed";
		f->open = 1;
		rmsg.qid = png.qid;
		rmsg.iounit = msize-IOHDRSZ;

		if(!obs_output_can_begin_data_capture(output, OBS_OUTPUT_VIDEO)) return "cannot begin capture";
		os_event_reset(pngready);
		print("Reset pngready\n");
		obs_output_begin_data_capture(output, OBS_OUTPUT_VIDEO);
		png.mtime = time(0);
		return nil;
	}
	return "file does not exist";
}

char *
rcreate(Fid *f){
	return "no creates allowed";
}

char *
rread(Fid *f){
	if(!f->used) return "rread: bad fid";
	if(!f->open) return "not opened";

	if(f->qid == &root.qid){
		/* A partial read should never happen and is illegal, but just in case */
		if(tmsg.offset > npngstat) return "bad offset (directory read)";
		print("Root read (Offset %d)\n", tmsg.offset);
		rmsg.data = (char*) &pngstat + tmsg.offset;
		rmsg.count = npngstat - tmsg.offset;
		return nil;
	}
	if(f->qid == &png.qid){
		u32int ossize;
		print("Waiting on png ready\n");
		os_event_wait(pngready);
		print("Finished waiting\n");
		png.atime = time(0);
		if(tmsg.offset >= *pngsizep){
			print("PNG read offset too large (%d >= %d, Count %d)\n", tmsg.offset, *pngsizep, tmsg.count);
			rmsg.count = 0;
			rmsg.data = nil;
			return nil;
		}
		ossize = *pngsizep - tmsg.offset;
		rmsg.count = ossize < tmsg.count ? ossize : tmsg.count;
		rmsg.data = *pngdatapp + tmsg.offset;
		print("PNG read (Offset %d, %d of %d)\n", rmsg.data - *pngdatapp, rmsg.count, tmsg.count);
		return nil;
	}
	return "file does not exist";
}

char *
rwrite(Fid *f){
	return "no writing allowed";
}

char *
rclunk(Fid *f){
	print("Clunking FID #%d\n", f->fid);
	if(!f->used) return "rclunk: bad fid";
	f->used = 0;
	f->open = 0;
	f->qid = nil;
	return nil;
}

char *
rremove(Fid *f){
	return "no removing allowed";
}

char *
rstat(Fid *f){
	if(!f->used) return "rstat: bad fid";
	if(f->dir == &root){
		rmsg.nstat = nrootstat;
		rmsg.stat = rootstat;
		return nil;
	}
	if(f->dir == &png){
		rmsg.nstat = npngstat;
		rmsg.stat = pngstat;
		return nil;
	}
	return "file does not exist";
}

char *
rwstat(Fid *f){
	return "no wstats allowed";
}

static void
initdirs(void){
	char *user = getuser();

	root.qid = (Qid){ (uvlong) 0, (ulong) 0, QTDIR };
	root.mode = DMDIR | 0555;
	root.atime = time(0);
	root.mtime = root.atime;
	root.length = 0;
	root.name = "/";
	root.uid = user;
	root.gid = user;
	root.muid = user;
	nrootstat = convD2M(&root, rootstat, sizeof rootstat);

	png.qid = (Qid){ (uvlong) 1, (ulong) 0, QTFILE };
	png.mode = 0444;
	png.atime = time(0);
	png.mtime = png.atime;
	png.length = 0;
	png.name = "png";
	png.uid = user;
	png.gid = user;
	png.muid = user;
	print("ngpnstat: %d\n", npngstat = convD2M(&png, pngstat, sizeof pngstat));
}

static void
sigerror(char *m, int code){
	print("screenshot: %s - %r\n", m);
	obs_output_signal_stop(output, code);
	pthread_exit(NULL);
}

void *
handle9p(void *data){
	char testdata[] = "THIS IS EVEN MORE TEST DATA ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	u32int testsize = sizeof testdata;
	ssdata *d = data;
	int n;
	char *err;

	if(post9pservice(d->p[1], d->sname, nil) < 0) sigerror("post9pservice failed", OBS_OUTPUT_CONNECT_FAILED);
	close(d->p[1]);
	pngready = d->ready;
	pngdatapp = &d->data;
	pngsizep = &(d->size);
	initdirs();

	for(;;){
		n = read9pmsg(d->p[0], mdata, msize);
		if(n < 0) sigerror("read9pmsg error", OBS_OUTPUT_ERROR);
		if(n == 0) sigerror("read9pmsg eof", OBS_OUTPUT_DISCONNECTED);

		if(convM2S(mdata, n, &tmsg) == 0) continue;

		if(!rfuncs[tmsg.type]) err = "Bad T-message type";
		else err = (*rfuncs[tmsg.type])(newfid(tmsg.fid));

		if(err){
			rmsg.type = Rerror;
			rmsg.ename = err;
		} else {
			rmsg.type = tmsg.type+1;
			rmsg.fid = tmsg.fid;
		}
		rmsg.tag = tmsg.tag;

		n = convS2M(&rmsg, mdata, msize);
		if(n == 0){
			err = smprint("convS2m error on write (msize: %d)", msize);
			sigerror(err, OBS_OUTPUT_ERROR);
			free(err);
			err = nil;
		}
		if(write(d->p[0], mdata, n) != n) sigerror("error writing rmsg to pipe", OBS_OUTPUT_ERROR);
	}
}
