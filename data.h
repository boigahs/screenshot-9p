typedef struct video_scale_info vsi;
typedef struct
ssdata {
	obs_output_t *output;
	vsi *conv;
	char *sname;
	int p[2];
	pthread_t ht;
	int w;
	int h;
	os_event_t *ready;
	char *data;
	u32int size;
} ssdata;
